﻿using System.Web.Mvc;
using Autofac;
using LFWD.Services.Implementations;
using LFWD.Services.Interfaces;
using System.Reflection;
using LFWD.Data.Context;
using Autofac.Integration.Mvc;
using LFWD.Web.Security;
using LFWD.Web.Interfaces;

namespace LFWD.Web.App_Start
{
    public class AutofacConfig
    {
        public static void Setup()
        {
            //container builder init
            var builder = new ContainerBuilder();

            // Get your HttpConfiguration.
            //var config = GlobalConfiguration.Configuration;


            // Register your web controllers.
            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            //register database context
            builder.RegisterType<LfwdContext>().As<ILfwdContext>();

            //register all services in VT.Services
            builder.RegisterType<UserService>().As<IUserService>();

            //register Web components
            builder.RegisterType<UserAuthenticator>().As<IUserAuthenticator>();

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();

            // Set the dependency resolver for MVC.
            var mvcResolver = new AutofacDependencyResolver(container);
            DependencyResolver.SetResolver(mvcResolver);

            builder.RegisterModule(new AutofacWebTypesModule());
            builder.RegisterFilterProvider();
        }
    }
}