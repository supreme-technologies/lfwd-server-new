﻿using AutoMapper;
using LFWD.Services.DTOs.UserRequestResponse;
using LFWD.Web.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LFWD.Web.App_Start
{
    public class AutomapConfig
    {
        public static void Setup()
        {
            Mapper.Initialize(config =>
            {
               
                config.CreateMap<UserModel, AddUserRequest>().ReverseMap(); 

            });
        }
    }
}