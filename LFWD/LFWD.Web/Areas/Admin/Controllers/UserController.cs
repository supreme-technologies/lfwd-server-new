﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LFWD.Data.Entities;
using LFWD.Services.DTOs.UserRequestResponse;
using LFWD.Services.Interfaces;
using LFWD.Web.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LFWD.Web.Areas.Admin.Controllers
{
    public class UserController : Controller
    {
        #region Fields
        private readonly IUserService _userService;
        #endregion


        #region Constructor
        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        #endregion
        
        #region Methods
        
        // GET: Admin/User
        public ActionResult Index()
        {
            return View("Index");
        }


        //GET: UsersList
        [HttpPost]
        [Route("~/Users/UserList")]
        public ActionResult UserList([DataSourceRequest] DataSourceRequest request)
        {
            var data = GetUsersList();
            return Json(data.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [Route("~/Users/SaveUser")]
        public ActionResult SaveUser(UserModel model)
        {
            var data = AutoMapper.Mapper.Map<AddUserRequest>(model);
            var response = _userService.Add(data);
            return Json(new
            {
                success = response.Success,
                Message = response.Message
            });
        }


        [HttpPost]
        [Route("~/Users/UpdateUser")]
        public ActionResult UpdateUser(UserModel model)
        {

            var data = AutoMapper.Mapper.Map<AddUserRequest>(model);
            var response = _userService.Update(data);
            return Json(new
            {
                success = response.Success,
                Message = response.Message
            });
        }



        [HttpPost]
        [Route("~/Users/DeleteUser")]
        public ActionResult DeleteUser(DeleteUserModel model)
        {
            var response = _userService.Delete(model.Id);
            return Json(new
            {
                success = response.Success,
                Message = response.Message
            });
        }


        //Edit User
        [HttpPost]
        [Route("~/Users/EditUser/{id}")]
        public ActionResult EditUser(Guid id)
        {
            var response = _userService.GetUser(id);
            var model = new User
            {
                UserId = response.UserId,
                FirstName = response.FirstName,
                LastName = response.LastName,
                Username = response.Username,
                Email = response.Email,
                IsActive = response.IsActive
            };
            return Json(model);
        }


        public IEnumerable<GetUser> GetUsersList()
        {
            var userList = _userService.GetAllUsers();
            return userList;
        }

        #endregion
    }
}