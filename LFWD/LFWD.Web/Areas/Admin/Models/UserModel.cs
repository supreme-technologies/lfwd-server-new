﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LFWD.Web.Areas.Admin.Models
{
    public class UserModel
    {
        public Guid UserId { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public int Role { get; set; }
        public string HashedPassword { get; set; }
        public string PasswordSalt { get; set; }
    }
}