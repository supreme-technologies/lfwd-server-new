﻿using System;

namespace LFWD.Web.Areas.Admin.Models
{

    public class DeleteUserModel
    {
        public Guid Id { get; set; }
    }
}