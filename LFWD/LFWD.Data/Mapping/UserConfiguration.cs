﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity;
using LFWD.Data.Entities;

namespace LFWD.Data.Mapping
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration(string schema = "ifworkdaydb")
        {
            ToTable(schema + ".user");
            HasKey(x => x.UserId);
            Property(x => x.Email).HasColumnName("Email").IsRequired().HasMaxLength(50);
            Property(x => x.HashedPassword).HasColumnName("HashedPassword").IsRequired().HasMaxLength(100);
            Property(x => x.FirstName).HasColumnName("FirstName").IsRequired().HasMaxLength(50);
            Property(x => x.LastName).HasColumnName("LastName").IsRequired().HasMaxLength(50);
            Property(x => x.Role).HasColumnName("Role").IsOptional();
            Property(x => x.PasswordSalt).HasColumnName("PasswordSalt").IsRequired().HasMaxLength(100);
            Property(x => x.IsActive).HasColumnName("IsActive").IsRequired();
            Property(x => x.CreatedOn).HasColumnName("CreatedOn").IsOptional();
            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsOptional();
            Property(x => x.UpdatedOn).HasColumnName("UpdatedOn").IsOptional();
            Property(x => x.UpdatedBy).HasColumnName("UpdatedBy").IsOptional();
        }
    }
}
