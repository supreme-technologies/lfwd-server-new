﻿using System.Data.Entity.ModelConfiguration;
using LFWD.Data.Entities;

namespace LFWD.Data.Mapping
{
    public class CustomerConfiguration : EntityTypeConfiguration<Customer>
    {
        public CustomerConfiguration(string schema = "lfworkdaydb")
        {
            ToTable(schema + ".customer");
            HasKey(x => x.CustomerId);

            Property(x => x.Firstname).HasColumnName("Firstname").IsOptional().HasMaxLength(50);
            Property(x => x.Lastname).HasColumnName("Lastname").IsOptional().HasMaxLength(50);
            Property(x => x.Firstname).HasColumnName("Firstname").IsOptional().HasMaxLength(50);
            Property(x => x.Address).HasColumnName("Address").IsOptional().HasMaxLength(50);
            Property(x => x.Address1).HasColumnName("Address1").IsOptional().HasMaxLength(50);
            Property(x => x.City).HasColumnName("City").IsOptional().HasMaxLength(50);
            Property(x => x.Day).HasColumnName("Day").IsOptional();
            Property(x => x.State).HasColumnName("State").IsOptional().HasMaxLength(50);
            Property(x => x.Country).HasColumnName("Country").IsOptional();
            Property(x => x.ZipCode).HasColumnName("ZipCode").IsOptional();
            Property(x => x.Email).HasColumnName("Email").IsOptional();
            Property(x => x.IsActive).HasColumnName("IsActive").IsOptional();
            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsOptional();
            Property(x => x.CreatedOn).HasColumnName("CreatedOn").IsOptional();
            Property(x => x.UpdatedBy).HasColumnName("UpdatedBy").IsOptional();
            Property(x => x.UpdatedOn).HasColumnName("UpdatedOn").IsOptional();

        }
    }
}
