﻿using LFWD.Data.Entities;
using System.Data.Entity.ModelConfiguration;

namespace LFWD.Data.Mapping
{
    public class RegionConfiguration : EntityTypeConfiguration<Region>
    {
        public RegionConfiguration(string schema = "lfworkdaydb")
        {
            ToTable(schema + ".region");
            HasKey(x => x.Id);

            Property(x => x.Name).HasColumnName("Name").IsOptional().HasMaxLength(50);
            Property(x => x.IsActive).HasColumnName("IsActive").IsOptional();

            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsOptional();
            Property(x => x.CreatedOn).HasColumnName("CreatedOn").IsOptional();
            Property(x => x.UpdatedBy).HasColumnName("UpdatedBy").IsOptional();
            Property(x => x.UpdatedOn).HasColumnName("UpdatedOn").IsOptional();
        }
    }
}
