﻿using System.Data.Entity.ModelConfiguration;
using LFWD.Data.Entities;

namespace LFWD.Data.Mapping
{
    public class DeliveryConfiguration : EntityTypeConfiguration<Delivery>
    {
        public DeliveryConfiguration(string schema = "lfworkdaydb")
        {
            ToTable(schema + ".delivery");
            HasKey(x => x.DeliveryId);

            Property(x => x.CustomerId).HasColumnName("CustomerId").IsOptional();
            Property(x => x.FuelTypeId).HasColumnName("FuelTypeId").IsOptional();
            Property(x => x.NumberOfGallons).HasColumnName("NumberOfGallons").IsOptional();
            Property(x => x.Status).HasColumnName("Status").IsOptional().HasMaxLength(50);

            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsOptional();
            Property(x => x.CreatedOn).HasColumnName("CreatedOn").IsOptional();
            Property(x => x.UpdatedBy).HasColumnName("UpdatedBy").IsOptional();
            Property(x => x.UpdatedOn).HasColumnName("UpdatedOn").IsOptional();

        }
    }
}
