﻿using System.Data.Entity.ModelConfiguration;
using LFWD.Data.Entities;

namespace LFWD.Data.Mapping
{
    public class ColorTypeConfiguration : EntityTypeConfiguration<ColorType>
    {
        public ColorTypeConfiguration(string schema = "lfworkdaydb")
        {
            ToTable(schema + ".colortype");
            HasKey(x => x.ColorTypeId);

            Property(x => x.Title).HasColumnName("Title").IsOptional().HasMaxLength(50);
            Property(x => x.IsActive).HasColumnName("IsActive").IsOptional();

            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsOptional(); 
            Property(x => x.CreatedOn).HasColumnName("CreatedOn").IsOptional();
            Property(x => x.UpdatedBy).HasColumnName("UpdatedBy").IsOptional();
            Property(x => x.UpdatedOn).HasColumnName("UpdatedOn").IsOptional(); 
        }
    }
}
