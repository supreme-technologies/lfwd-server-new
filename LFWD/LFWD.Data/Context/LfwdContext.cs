﻿using System; 
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq; 
using LFWD.Data.Entities;
using LFWD.Data.Mapping;

namespace LFWD.Data.Context
{
    public class LfwdContext : DbContext, ILfwdContext
    {
        #region Constructor

        public LfwdContext()
            : base("name=ConnectionString")
        {
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 180;
        }

        public LfwdContext(DbConnection existingConnection, bool contextOwnsConnection)
            : base(existingConnection, contextOwnsConnection)
        {
        }

        public LfwdContext(DbConnection existingConnection)
            : base(existingConnection, true)
        {
        }

        #endregion


        #region Table/Views  

        public IDbSet<ColorType> ColorTypes { get; set; }
        public IDbSet<ClarityType> ClarityTypes { get; set; }
        public IDbSet<Customer> Customers { get; set; }
        public IDbSet<Delivery> Deliveries { get; set; }
        public IDbSet<User> Users { get; set; }
        public IDbSet<FuelType> FuelTypes { get; set; }
        public IDbSet<Region> Regions { get; set; } 

        #endregion


        #region Overridable method(s)

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new ColorTypeConfiguration());
            modelBuilder.Configurations.Add(new DeliveryConfiguration());
            modelBuilder.Configurations.Add(new ClarityTypeConfiguration());
            modelBuilder.Configurations.Add(new DeliveryConfiguration());
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new FuelTypeConfiguration());
            modelBuilder.Configurations.Add(new RegionConfiguration());

        } 

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message, exception.InnerException);
            }
        }

        #endregion


        public Database Db
        {
            get { return this.Database; }
        }
    }
}
