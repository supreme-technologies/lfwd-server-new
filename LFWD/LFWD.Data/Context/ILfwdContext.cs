﻿using System;
using System.Data.Entity;
using LFWD.Data.Entities;

namespace LFWD.Data.Context
{
    public interface ILfwdContext 
    {
        Database Db { get; }
        IDbSet<ColorType> ColorTypes { get; set; }
        IDbSet<ClarityType> ClarityTypes { get; set; }
        IDbSet<Customer> Customers { get; set; }
        IDbSet<Delivery> Deliveries { get; set; }
        IDbSet<User> Users { get; set; }
        IDbSet<FuelType> FuelTypes { get; set; }
        IDbSet<Region> Regions { get; set; }
        int SaveChanges();
    }
}
