﻿using System; 

namespace LFWD.Data.Entities
{
    public class Delivery
    {
        public int DeliveryId { get; set; }
        public int CustomerId { get; set; }
        public int FuelTypeId { get; set; }
        public int NumberOfGallons { get; set; }
        public string Status { get; set; } 
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}
