﻿using System;

namespace LFWD.Data.Entities
{
    public class Region
    {
        public int Id { get; set; }
        public string Name { get; set; } 
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}
