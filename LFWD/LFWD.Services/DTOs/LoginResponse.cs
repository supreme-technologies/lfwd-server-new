﻿using LFWD.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFWD.Services.DTOs
{
    public class LoginResponse : BaseResponse
    {
        public User User { get; set; }
    }
}
