﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFWD.Services.DTOs
{
    public class LoginRequest
    {
        public string Email { get; set; }
    }
}
