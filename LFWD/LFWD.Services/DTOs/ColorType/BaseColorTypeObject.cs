﻿using System;

namespace LFWD.Services.DTOs.ColorType
{
    public class BaseColorTypeObject
    {
        public int ColorTypeId { get; set; }
        public string Title { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}
