﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFWD.Services.DTOs.UserRequestResponse
{
    public class AddUserResponse : BaseResponse
    {
    }

    public class UpdateUserResponse : BaseResponse
    {
    }

    public class UserAccessToCustomerResponse : BaseResponse
    {
    }
}
