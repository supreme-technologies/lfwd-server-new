﻿using System;
using System.Collections.Generic;

namespace LFWD.Services.DTOs.UserRequestResponse
{
    public class BaseUserRequest
    {
        public Guid UserId { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public int Role { get; set; }
        public string Password { get; set; }
    }

    public class AddUserRequest : BaseUserRequest
    {
    }

    public class UpdateUserRequest : BaseUserRequest
    {

    }

    public class UserAccessToCustomerRequest
    {
        public Guid UserId { get; set; }
        public List<Guid> Customers { get; set; }
    }
}
