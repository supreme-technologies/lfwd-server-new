﻿using LFWD.Data.Entities;
using LFWD.Services.DTOs;
using LFWD.Services.DTOs.Region;
using System.Collections.Generic;

namespace LFWD.Services.Interfaces
{
    public interface IRegionService
    {
        RegionResponse SaveRegion(RegionRequest request);
        IEnumerable<BaseRegionObject> GetRegionList();
        BaseResponse DeleteRegion(int id);
        Region GetRegion(int? id); 
    }
}
