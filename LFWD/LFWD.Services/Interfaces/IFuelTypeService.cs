﻿using LFWD.Data.Entities;
using LFWD.Services.DTOs;
using LFWD.Services.DTOs.FuelType;
using System.Collections.Generic;

namespace LFWD.Services.Interfaces
{
    public interface IFuelTypeService
    {
        FuelTypeResponse SaveFuelType(FuelTypeRequest request);
        IEnumerable<BaseFuelTypeObject> GetFuelTypeList();
        BaseResponse DeleteFuelType(int id); 
        FuelType GetFuelType(int? id);
    }
}
