﻿using LFWD.Data.Entities;
using LFWD.Services.DTOs;
using LFWD.Services.DTOs.ColorType;
using System.Collections.Generic;

namespace LFWD.Services.Interfaces
{
    public interface IColorTypeService
    {
        ColorTypeResponse SaveColorType(ColorTypeRequest request);
        IEnumerable<BaseColorTypeObject> GetColorTypeList();
        BaseResponse DeleteColorType(int id);
        ColorType GetColorType(int? id);
    } 
}
