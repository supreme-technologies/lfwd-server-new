﻿using LFWD.Data.Entities;
using LFWD.Services.DTOs;
using LFWD.Services.DTOs.UserRequestResponse;
using System;
using System.Collections.Generic;

namespace LFWD.Services.Interfaces
{
    public interface IUserService
    {
        AddUserResponse Add(AddUserRequest request);
        AddUserResponse Update(AddUserRequest request);
        BaseResponse Delete(Guid userId);
        User GetUser(Guid userId);
        LoginResponse AuthenticateUser(LoginRequest request);
        IEnumerable<GetUser> GetAllUsers();
    }
}
