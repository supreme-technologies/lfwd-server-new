﻿using LFWD.Data.Entities;
using LFWD.Services.DTOs;
using LFWD.Services.DTOs.ClarityType;
using System.Collections.Generic;

namespace LFWD.Services.Interfaces
{
    public interface IClarityTypeService
    {
        ClarityTypeResponse SaveClarityType(ClarityTypeRequest request);
        IEnumerable<BaseClarityTypeObject> GetClarityTypeList(); 
        BaseResponse DeleteClarityType(int id);
        ClarityType GetClarityType(int? id);
    }
}
