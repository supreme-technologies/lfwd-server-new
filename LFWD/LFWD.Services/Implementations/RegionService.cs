﻿using LFWD.Data.Context;
using LFWD.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using LFWD.Data.Entities;
using LFWD.Services.DTOs;
using LFWD.Services.DTOs.Region;

namespace LFWD.Services.Implementations
{
    public class RegionService : IRegionService
    {
        #region Fields

        private readonly ILfwdContext _context;

        #endregion

        #region Construction

        public RegionService(ILfwdContext context)
        {
            _context = context;
        }

        #endregion

        #region Implementation

        //Delete Region
        public BaseResponse DeleteRegion(int id)
        {
            var response = new BaseResponse();

            try
            {
                var region = _context.Regions.FirstOrDefault(x => x.Id == id);

                if (region != null)
                {
                    _context.Regions.Remove(region);
                    _context.SaveChanges();
                    response.Success = true;
                    response.Message = "Region has been successfully deleted.";
                }
                else
                {
                    response.Success = false;
                    response.Message = "An error Occurred : Invalid Parameter.";
                }
            }
            catch (Exception exception)
            {
                response.Success = false;
                response.Message = "An error Occurred :" + exception.Message;
            }
            return response;
        }

        //Get Region
        public Region GetRegion(int? id)
        {
            return _context.Regions.FirstOrDefault(x => x.Id == id);
        }

        //Get All Regions
        public IEnumerable<BaseRegionObject> GetRegionList()
        {
            var region = _context.Regions.Select(x => new BaseRegionObject
            {
                Id = x.Id,
                Name = x.Name,
                IsActive = x.IsActive
            }).ToList();
            return region;
        }

        //Save region
        public RegionResponse SaveRegion(RegionRequest request)
        {
            var response = new RegionResponse();

            try
            {
                var region = _context.Regions.FirstOrDefault(x => x.Id == request.Id);

                if (region == null)
                {
                    region = new Region          // ADD
                    {
                        Id = request.Id,
                        Name = request.Name,
                        IsActive = request.IsActive,
                        CreatedBy = request.CreatedBy,
                        CreatedOn = DateTime.UtcNow,
                        UpdatedBy = request.UpdatedBy,
                        UpdatedOn = DateTime.UtcNow
                    };

                    _context.Regions.Add(region);
                }
                else    //EDIT
                {
                    region.Name = request.Name;
                    region.IsActive = request.IsActive;
                    region.CreatedBy = request.CreatedBy;
                    region.CreatedOn = request.CreatedOn;
                    region.UpdatedBy = request.UpdatedBy;
                    region.UpdatedOn = request.UpdatedOn;
                }

                _context.SaveChanges();
                response.Success = true;
                response.Message = "Region has been successfully saved.";
            }
            catch (Exception exception)
            {
                response.Message = exception.ToString();
            }
            return response;
        }

        #endregion
    }
}
