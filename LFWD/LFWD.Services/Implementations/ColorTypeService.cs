﻿using LFWD.Data.Context;
using LFWD.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using LFWD.Data.Entities;
using LFWD.Services.DTOs;
using LFWD.Services.DTOs.ColorType;

namespace LFWD.Services.Implementations
{
    public class ColorTypeService : IColorTypeService
    {
        #region Fields

        private readonly ILfwdContext _context;

        #endregion

        #region Constructor

        public ColorTypeService(ILfwdContext context)
        {
            _context = context;
        }

        #endregion

        #region Implementation

        //Delete ColorType
        public BaseResponse DeleteColorType(int id)
        {
            var response = new BaseResponse();

            try
            {
                var colorType = _context.ColorTypes.FirstOrDefault(x => x.ColorTypeId == id);

                if (colorType != null)
                {
                    _context.ColorTypes.Remove(colorType);
                    _context.SaveChanges();
                    response.Success = true;
                    response.Message = "ColorType has been successfully deleted.";
                }
                else
                {
                    response.Success = false;
                    response.Message = "An error Occurred : Invalid Parameter.";
                }
            }
            catch (Exception exception)
            {
                response.Success = false;
                response.Message = "An error Occurred :" + exception.Message;
            }
            return response;
        }

        //Get ColorType
        public ColorType GetColorType(int? id)
        {
            return _context.ColorTypes.FirstOrDefault(x => x.ColorTypeId == id);
        }

        //Get All ColorType
        public IEnumerable<BaseColorTypeObject> GetColorTypeList()
        {
            var colorType = _context.ColorTypes.Select(x => new BaseColorTypeObject
            {
                ColorTypeId = x.ColorTypeId,
                Title = x.Title,
                IsActive = x.IsActive
            }).ToList();
            return colorType;
        }

        //Save ColorType
        public ColorTypeResponse SaveColorType(ColorTypeRequest request)
        {
            var response = new ColorTypeResponse();

            try
            {
                var colorType = _context.ColorTypes.FirstOrDefault(x => x.ColorTypeId == request.ColorTypeId);

                if (colorType == null)
                {
                    colorType = new ColorType          // ADD
                    {
                        ColorTypeId = request.ColorTypeId,
                        Title = request.Title,
                        IsActive = request.IsActive,
                        CreatedBy = request.CreatedBy,
                        CreatedOn = DateTime.UtcNow,
                        UpdatedBy = request.UpdatedBy,
                        UpdatedOn = DateTime.UtcNow
                    };

                    _context.ColorTypes.Add(colorType);
                }
                else    //EDIT
                {
                    colorType.Title = request.Title;
                    colorType.IsActive = request.IsActive;
                    colorType.CreatedBy = request.CreatedBy;
                    colorType.CreatedOn = request.CreatedOn;
                    colorType.UpdatedBy = request.UpdatedBy;
                    colorType.UpdatedOn = request.UpdatedOn;
                } 
                _context.SaveChanges();
                response.Success = true;
                response.Message = "Color has been successfully saved.";
            }
            catch (Exception exception)
            {
                response.Message = exception.ToString();
            }
            return response;
        } 
    }
}

#endregion