﻿using LFWD.Data.Context;
using LFWD.Data.Entities;
using LFWD.Services.DTOs;
using LFWD.Services.Interfaces;
using System;
using LFWD.Common;
using LFWD.Services.DTOs.UserRequestResponse;
using System.Collections.Generic;
using System.Linq;
using LFWD.Common.Utils;

namespace LFWD.Services.Implementations
{
    public class UserService : IUserService
    {
        #region Fields

        private readonly ILfwdContext _context;

        #endregion 

        #region Constructor

        public UserService(ILfwdContext context)
        {
            _context = context;
        }

        #endregion

        #region Interface Implementation

        //Get User
        public User GetUser(Guid userId)
        {
            //find out user with id
            var user = _context.Users.Where(x => !x.IsActive).FirstOrDefault(x => x.UserId == userId);

            if (user == null) //User not found
            {
                throw new Exception("User does not exist in the database");
            }
            return new User
            {
                UserId = user.UserId,
                Email = user.Email,
                FirstName = user.FirstName,
                Role = (int)user.Role,
                PasswordSalt = user.PasswordSalt,
                IsActive = user.IsActive,
            };
        }

        //Delete User
        public BaseResponse Delete(Guid userId)
        {
            var response = new BaseResponse();
            try
            {

                //Find out user with userId

                var user = _context.Users.FirstOrDefault(x => x.UserId == userId);

                if (user != null) //Found user
                {
                    _context.Users.Remove(user);
                    _context.SaveChanges(); response.Success = true;
                    response.Message = "User has been successfully deleted.";
                }
                else   // not found
                {
                    response.Success = false;
                    response.Message = "An error Occurred : Invalid Parameter.";
                }
            }
            catch (Exception exception)
            {
                response.Message = exception.ToString();
            }

            return response;
        }

        // Get User
        public IEnumerable<GetUser> GetAllUsers()
        {
            //Get Users list 
            var users = _context.Users.Where(y => y.Role != (int)UserRoles.Admin)
                .Select(x => new GetUser
                {
                    UserId = x.UserId,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email,
                    Role = x.Role,
                    IsActive = x.IsActive
                });
            return users;

        }

        //Create New user
        public AddUserResponse Add(AddUserRequest request)
        {
            request.UserId = Guid.NewGuid();

            var response = new AddUserResponse();

            var passwordSalt = PasswordUtil.GenerateSalt();
            var hashedPassword = PasswordUtil.CreatedHashedPassword(request.Password,
                passwordSalt);

            try
            {
                //Try to find out user with request email.
                var checkUser = _context.Users.FirstOrDefault(x => x.UserId == request.UserId);

                if (checkUser != null)
                {
                    response.Message = "User with this email already exists.";
                    response.Success = false;
                    return response;
                }
                else
                {
                    // Save user
                    var newUser = new User
                    {
                        UserId = Guid.NewGuid(),
                        FirstName = request.FirstName,
                        LastName = request.LastName,
                        Email = request.Email,
                        IsActive = request.IsActive,
                        Role = request.Role,
                        Username = request.Username,
                        HashedPassword = hashedPassword,
                        PasswordSalt = passwordSalt
                    };

                    _context.Users.Add(newUser);

                    response.Success = true;
                    response.Message = "User has been successfully saved.";
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
            }
            return response;
        }

        //Update user
        public AddUserResponse Update(AddUserRequest request)
        {

            var response = new AddUserResponse();

            try
            {
                var existingUser = GetUser(request.UserId);

                if (existingUser == null)
                {
                    response.Message = "This user does not exist.";
                    return response;
                }
                existingUser.FirstName = request.FirstName;
                existingUser.LastName = request.LastName;
                existingUser.IsActive = request.IsActive;
                existingUser.Email = request.Email;
                existingUser.Username = request.Username;
                existingUser.Role = request.Role;
                _context.SaveChanges();

                response.Success = true;
                response.Message = "Profile information has been updated successfully.";
            }
            catch (Exception exception)
            {
                response.Message = "An error occured while updating data";
            }
            return response;
        }

        //Authenticate User
        public LoginResponse AuthenticateUser(LoginRequest request)
        {
            {
                var response = new LoginResponse();

                var existingUser = _context.Users.FirstOrDefault(x => x.Email == request.Email);

                if (existingUser == null)
                {
                    response.Message = "User with this email does not exist.";
                }
                else
                {
                    response.Success = true;
                    response.User = existingUser;
                }
                return response;
            }
        } 

        #endregion
    }
}
