﻿using LFWD.Data.Context;
using LFWD.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using LFWD.Data.Entities;
using LFWD.Services.DTOs;
using LFWD.Services.DTOs.FuelType;

namespace LFWD.Services.Implementations
{
    public class FuelTypeService : IFuelTypeService
    {
        #region Fields

        private readonly ILfwdContext _context;

        #endregion

        #region Contructor

        public FuelTypeService(ILfwdContext context)
        {
            _context = context;
        }

        #endregion

        #region Implementation

        //Delete FuelType
        public BaseResponse DeleteFuelType(int id)
        {
            var response = new BaseResponse();

            try
            {
                var fuelType = _context.FuelTypes.FirstOrDefault(x => x.FuelTypeId == id);

                if (fuelType != null)
                {
                    _context.FuelTypes.Remove(fuelType);
                    _context.SaveChanges();
                    response.Success = true;
                    response.Message = "FuelType has been successfully deleted.";
                }
                else
                {
                    response.Success = false;
                    response.Message = "An error Occurred : Invalid Parameter.";
                }
            }
            catch (Exception exception)
            {
                response.Success = false;
                response.Message = "An error Occurred :" + exception.Message;
            }
            return response;
        }

        //Get FuelType
        public FuelType GetFuelType(int? id)
        {
            return _context.FuelTypes.FirstOrDefault(x => x.FuelTypeId == id);
        }

        //Get All Fueltype
        public IEnumerable<BaseFuelTypeObject> GetFuelTypeList()
        {
            var fuelType = _context.FuelTypes.Select(x => new BaseFuelTypeObject
            {
                FuelTypeId = x.FuelTypeId,
                Title = x.Title,
                IsActive = x.IsActive
            }).ToList();
            return fuelType;
        }

        //Save FuelType
        public FuelTypeResponse SaveFuelType(FuelTypeRequest request)
        {
            var response = new FuelTypeResponse();

            try
            {
                var fuelType = _context.FuelTypes.FirstOrDefault(x => x.FuelTypeId == request.FuelTypeId);

                if (fuelType == null)
                {
                    fuelType = new FuelType          // ADD
                    {
                        FuelTypeId = request.FuelTypeId,
                        Title = request.Title,
                        IsActive = request.IsActive,
                        CreatedBy = request.CreatedBy,
                        CreatedOn = DateTime.UtcNow,
                        UpdatedBy = request.UpdatedBy,
                        UpdatedOn = DateTime.UtcNow
                    };

                    _context.FuelTypes.Add(fuelType);
                }
                else    //EDIT
                {
                    fuelType.Title = request.Title;
                    fuelType.IsActive = request.IsActive;
                    fuelType.CreatedBy = request.CreatedBy;
                    fuelType.CreatedOn = request.CreatedOn;
                    fuelType.UpdatedBy = request.UpdatedBy;
                    fuelType.UpdatedOn = request.UpdatedOn;
                }

                _context.SaveChanges();
                response.Success = true;
                response.Message = "FuelType has been successfully saved.";
            }
            catch (Exception exception)
            {
                response.Message = exception.ToString();
            }
            return response;
        }

        #endregion
    }
}
