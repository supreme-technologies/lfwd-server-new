﻿using LFWD.Data.Context;
using LFWD.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using LFWD.Data.Entities;
using LFWD.Services.DTOs;
using LFWD.Services.DTOs.ClarityType;

namespace LFWD.Services.Implementations
{
    public class ClarityTypeService : IClarityTypeService
    {
        #region Fields

        private readonly ILfwdContext _context;

        #endregion

        #region Constructor

        public ClarityTypeService(ILfwdContext context)
        {
            _context = context;
        }

        #endregion

        #region Implementation

        //Delete ClarityType
        public BaseResponse DeleteClarityType(int id)
        {
            var response = new BaseResponse();

            try
            {
                var clarityType = _context.ClarityTypes.FirstOrDefault(x => x.ClarityTypeId == id);

                if (clarityType != null)
                {
                    _context.ClarityTypes.Remove(clarityType);
                    _context.SaveChanges();
                    response.Success = true;
                    response.Message = "ClarityType has been successfully deleted.";
                }
                else
                {
                    response.Success = false;
                    response.Message = "An error Occurred : Invalid Parameter.";
                }
            }
            catch (Exception exception)
            {
                response.Success = false;
                response.Message = "An error Occurred :" + exception.Message;
            }
            return response;
        }

        //get ClarityType
        public ClarityType GetClarityType(int? id)
        {
            return _context.ClarityTypes.FirstOrDefault(x => x.ClarityTypeId == id);
        }

        //Get All ClarityType
        public IEnumerable<BaseClarityTypeObject> GetClarityTypeList()
        {
            var colorType = _context.ClarityTypes.Select(x => new BaseClarityTypeObject
            {
                ClarityTypeId = x.ClarityTypeId,
                Title = x.Title,
                IsActive = x.IsActive
            }).ToList();
            return colorType;
        }

        //Save ClarityType
        public ClarityTypeResponse SaveClarityType(ClarityTypeRequest request)
        {
            var response = new ClarityTypeResponse();

            try
            {
                var clarityType = _context.ClarityTypes.FirstOrDefault(x => x.ClarityTypeId == request.ClarityTypeId);

                if (clarityType == null)
                {
                    clarityType = new ClarityType          // ADD
                    {
                        ClarityTypeId = request.ClarityTypeId,
                        Title = request.Title,
                        IsActive = request.IsActive,
                        CreatedBy = request.CreatedBy,
                        CreatedOn = DateTime.UtcNow,
                        UpdatedBy = request.UpdatedBy,
                        UpdatedOn = DateTime.UtcNow
                    };

                    _context.ClarityTypes.Add(clarityType);
                }
                else    //EDIT
                {
                    clarityType.Title = request.Title;
                    clarityType.IsActive = request.IsActive;
                    clarityType.CreatedBy = request.CreatedBy;
                    clarityType.CreatedOn = request.CreatedOn;
                    clarityType.UpdatedBy = request.UpdatedBy;
                    clarityType.UpdatedOn = request.UpdatedOn;
                }

                _context.SaveChanges();
                response.Success = true;
                response.Message = "ClarityType has been successfully saved.";
            }
            catch (Exception exception)
            {
                response.Message = exception.ToString();
            }
            return response;
        }

        #endregion

    }
}
