﻿using System.ComponentModel;

namespace LFWD.Common
{
    public enum UserRoles
    {
        [Description("Admin")]
        Admin = 1,

        [Description("User")]
        User = 0
    }
    public enum ExportStatus
    {
        Valid,
        Invalid,
    }
}
