﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace LFWD.Common
{
    public class CustomIdentity : GenericIdentity
    {
        public CustomIdentity(string email, int id, string fullname, string role) : base(email)
        {
            UserId = id;
            FullName = fullname;
            Role = role;
        }

        /// <summary>
        ///     Generic Identity calls username "Name" which is the same as Email in our case
        ///     This property just makes it a bit more user friendly to get the email address
        /// </summary>
        public string EmailAddress
        {
            get { return Name; }
        }

        public int UserId { get; private set; }
        public string FullName { get; private set; }
        public string Role { get; private set; }
    }
}
