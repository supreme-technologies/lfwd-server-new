﻿using System.Configuration;
using System.Collections.Specialized;


namespace LFWD.Common
{
    public static class ApplicationSettings
    {
      

        public static string Username => ConfigurationManager.AppSettings["Username"];

        public static string Password => ConfigurationManager.AppSettings["Password"];
    }
}
